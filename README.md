# loki-promtail helmfile bundle

This bundle deploys loki and promtail with a best basic practice configuration and additional things like dashboards and datasources for loki-promtail.

## Table of Contents

[[_TOC_]]

## Deployment

### Via Gitlab CI

The following examples expects you to have the following prerequisites:

* You have a Gitlab Project
* You have two `KUBECONFIG` Gitlab CI variables defined which are masked (base64 encoded) and tagged with the matching environment: `production`/`stage`

Create the following directory/file structure in your (control) repository:
```bash
└── syseleven-managed-loki-promtail
    ├── .gitlab-ci.yml
    ├── values-loki-production.yaml (only if needed to overwrite settings)
    ├── values-loki-stage.yaml (only if needed to overwrite settings)
    └── values-loki.yaml (only if needed to overwrite settings)
    ├── values-promtail-production.yaml (only if needed to overwrite settings)
    ├── values-promtail-stage.yaml (only if needed to overwrite settings)
    └── values-promtail.yaml (only if needed to overwrite settings)
```

loki-promtail/.gitlab-ci.yml:
```yaml
include:
  - project: syseleven/helm-charts/loki-promtail
    file: LokiPromtail.yaml
    ref: master

managed-services-loki-promtail-diff-production:
  extends: .managed-services-loki-promtail-diff
  environment:
    name: production

managed-services-loki-promtail-deploy-production:
  extends: .managed-services-loki-promtail-deploy
  variables:
    CUSTOM_COMMAND: curl -d 'deployment done' https://rocketchat.syseleven.de
  environment:
    name: production

managed-services-loki-promtail-diff-stage:
  extends: .managed-services-loki-promtail-diff
  environment:
    name: stage

managed-services-loki-promtail-deploy-stage:
  extends: .managed-services-loki-promtail-deploy
  environment:
    name: stage
```

Fill in customer/environment specific configuration in syseleven-managed/loki-promtail/values-loki.yaml, and syseleven-managed-loki-promtail/values-loki-${ENVIRONMENT}.yaml.

syseleven-managed-loki-promtail/values-loki-stage.yaml:
```yaml
persistence:
  size: 5Gi
```

syseleven-managed-loki-promtail/values-loki-production.yaml:
```yaml
persistence:
  size: 10Gi
```

### Manual

You can also manually deploy the directory structure mentioned above. This might be useful for bootstraping, debugging or development.

### docker

```bash
cd syseleven-managed-loki-promtail

# Diff
docker run -v $PATH_TO_KUBECONFIG:/root/.kube/config -v $PWD:/src -e BASEDIR=/src registry.gitlab.com/syseleven/helm-charts/loki-promtail:${TAG:-latest} diff

# Apply
docker run -v $PATH_TO_KUBECONFIG:/root/.kube/config -v $PWD:/src -e BASEDIR=/src registry.gitlab.com/syseleven/helm-charts/loki-promtail:${TAG:-latest} apply
```

### helmfile

You can use helmfile locally to apply the helmfile bundle.

```bash
$ cd syseleven-managed-loki-promtail
$ cat helmfile.yaml
helmfiles:
  # Prefer to use version in ref
  - path: git::https://gitlab.com/syseleven/helm-charts/loki-promtail@helmfile.yaml?ref=master

# Diff
$ helmfile diff

# Apply
$ helmfile apply
```

## Manual tasks

### Scale loki volume

```sh
# Delete sts (pvc will remain)
kubectl delete sts loki

# Patch the PVC to e.g. 10Gi
kubectl patch pvc storage-loki-0 -p '{"spec":{"resources":{"requests":{"storage":"10Gi"}}}}' --type=merge

# adapt values.yaml (-stage or -prod) for the same size as the patch above
$EDITOR values-loki-{env}.yaml

# Redeploy sts by e.g. CI
```

## Release

* Bump variables.VERSION in LokiPromtail.yaml
* Tag git release
* Push git tag

## Usage disclaimer

The use of this bundle on non SysEleven infrastructure (e.g. MetaKube) is prohibited.
